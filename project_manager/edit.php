<!doctype html>

<?php require_once('inc/config.php'); ?>

<html class="no-js" lang="en">
    <?php require_once('template/head.php'); ?>
    <body>
      <?php require_once('template/header.php'); ?>



  <main class="main">

        <form class="form-edit" method="post" action="addproj.php">
          <ul>
            <li class="row medium-6 large-4 columns">
              <label for="name">Project name</label>
              <input class="form-edit-input" name="name" type="text" />
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="start">From date</label>
              <input class="form-edit-input" name="start" type="date" />
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="end">To date</label>
              <input class="form-edit-input" name="end" type="date" />
            </li>
            <li class="row medium-6 large-4 columns">
              <input class="form-edit-input submit"  type="submit"  value="Submit">
            </li>
          </ul>
        </form>

      </main>

    </body>
</html>
