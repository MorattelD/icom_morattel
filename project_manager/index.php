<!doctype html>

<?php require_once('inc/config.php'); ?>

<?php
$stmt = $db->prepare("SELECT * FROM project");
$stmt->execute();
$projects= $stmt->fetchAll();
?>

<html class="no-js" lang="en">
  <?php require_once('template/head.php'); ?>
  <body>
    <?php require_once('template/header.php'); ?>

  <main class="main">
    <ul class="list">
      <li class="row">
        <span class="list-proj-name"></span>
        <span class="list-proj-month">Jan.</span>
        <span class="list-proj-month">Fév.</span>
        <span class="list-proj-month">Mar.</span>
        <span class="list-proj-month">Avr.</span>
        <span class="list-proj-month">mai</span>
        <span class="list-proj-month">juin.</span>
        <span class="list-proj-month">Juil.</span>
        <span class="list-proj-month">Août</span>
        <span class="list-proj-month">Sept.</span>
        <span class="list-proj-month">Oct.</span>
        <span class="list-proj-month">Nov.</span>
        <span class="list-proj-month">Dec.</span>
        <span class="list-proj-del"></span>
    </li>
        <?php foreach ($projects as $proj): ?>
          <li class="row list-proj">
            <span class="list-proj-name"><?php echo $proj['name']; ?></span>
            <?php
              $monthStart = date("n",strtotime($proj['start']));
              $monthEnd = date("n",strtotime($proj['end']));
             ?>
            <?php for($x = 0; $x <= 12; $x++): ?>
              <span class="list-proj-month">
                <?php if ($monthStart == $x): ?>
                  <i class="start-month">&#8226</i>
                <?php elseif($monthEnd == $x): ?>
                  <i class="end-month">&#8226</i>
                <?php endif; ?>
              </span>
            <?php endFor; ?>

            <span class="list-proj-del"><a href="delete.php?id=<?php echo $proj['id']; ?>">supprimer</a></span>
        <?php endForeach; ?>
    </ul>

    <a id="btn-add" href="edit.php">ajouter un projet</a>

  </main>

  </body>
</html>
