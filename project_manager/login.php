<!doctype html>

<html class="no-js" lang="en">
  <?php require_once('template/head.php'); ?>
  <body>

  <main class="main">
    <form class="form-edit" method="post" action="register.php">
      <ul>
        <li class="row medium-6 large-4 columns">
          <label for="user">Username</label>
          <input class="form-edit-input" name="user" type="text"/>
        </li>
        <li class="row medium-6 large-4 columns">
          <label for="pass">Password</label>
          <input class="form-edit-input" name="pass" type="pass"/>
        </li>

        <li class="row medium-6 large-4 columns">
          <input class="form-edit-input submit"  type="submit"  value="Submit">
        </li>
      </ul>
    </form>
  </main>

  </body>
</html>
